package com.affymetrix.genoviz.comparator;

import com.affymetrix.genoviz.bioviews.GlyphI;
import java.util.Comparator;

public final class GlyphMinXComparator implements Comparator<GlyphI> {

    @Override
    public int compare(GlyphI g1, GlyphI g2) {
        return Double.compare(g1.getCoordBox().x, g2.getCoordBox().x);
    }
}
