package com.affymetrix.genoviz.glyph;
import com.affymetrix.genoviz.bioviews.ViewI;
import com.affymetrix.genoviz.util.NeoConstants;

import java.awt.*;
import java.awt.geom.Path2D;
public class IntronGlyph extends DirectedGlyph {

    private static final int buffer_pixel = 10;
    private int x[];
    private int y[];
    private final Rectangle stem = new Rectangle();
    private static final int MAX_STEM_WIDTH = 25;
    private int stemWidth, headX, headY;
    private final Polygon poly;

    protected boolean fillArrowHead = true;
    protected boolean fillStem = true;

    public IntronGlyph() {
        x = new int[6];
        y = new int[6];
        poly = new Polygon(x, y, 3);
        setStemWidth(2);
    }

    private void calHead() {
        switch (this.getOrientation()) {
            case NeoConstants.HORIZONTAL:
                this.headY = getPixelBox().height;
                break;
            case NeoConstants.VERTICAL:
                this.headY = getPixelBox().width;
                break;
        }
        this.headX = this.headY / 2;
    }
    private void setStemWidth(int theWidth) {
        this.stemWidth = Math.max(Math.min(theWidth, MAX_STEM_WIDTH), 1);
        if (0 == this.stemWidth % 2) {
            this.stemWidth--;
        }
        this.headY = 2 * this.stemWidth + 4;
        this.headX = this.headY / 2;
    }

    @Override
    public void draw(ViewI view) {
        double hold_y = getCoordBox().y;

        getCoordBox().y += (getCoordBox().height / 2);
        view.transformToPixels(getCoordBox(), getPixelBox());
        int offset_center = getPixelBox().y;
        getCoordBox().y = hold_y;
        view.transformToPixels(getCoordBox(), getPixelBox());

        calHead();
        boolean zoomed = true;
        if (headY < 5
                || getPixelBox().x + getPixelBox().width / 2 + headX / 2 + buffer_pixel > getPixelBox().x + getPixelBox().width
                || getPixelBox().x + getPixelBox().width / 2 - headX / 2 - buffer_pixel < getPixelBox().x) {
            zoomed = false;
        }else{
            zoomed = true;
        }
        Graphics2D g = view.getGraphics();
        g.setColor(getBackgroundColor());
        switch (this.getDirection()) {
            case EAST:  // forward strand
                this.stem.x = getPixelBox().x;
                this.stem.y = offset_center;
                this.stem.width = getPixelBox().width;
                this.stem.height = stemWidth;
                if(zoomed)
                drawArrowHead(g, getPixelBox().x + getPixelBox().width / 2 + headX / 2,
                        getPixelBox().x + getPixelBox().width / 2 - headX / 2,
                        offset_center, getPixelBox().x + getPixelBox().width / 2);
                break;
            case WEST:
                this.stem.x = getPixelBox().x + headX;
                this.stem.y = offset_center - stemWidth ;
                this.stem.width = getPixelBox().width - headX;
                this.stem.height = stemWidth;
                if(zoomed)
                drawArrowHead(g, getPixelBox().x + getPixelBox().width / 2 - headX / 2,
                        getPixelBox().x + getPixelBox().width / 2 + headX / 2,
                        offset_center, getPixelBox().x + getPixelBox().width / 2);
                break;
            case SOUTH:  // forward strand
                this.stem.x = getPixelBox().x + (getPixelBox().width - stemWidth) / 2;
                this.stem.y = getPixelBox().y;
                this.stem.width = stemWidth;
                this.stem.height = getPixelBox().height - headX;
                if(zoomed)
                drawArrowHead(g, getPixelBox().y + getPixelBox().height / 2 + headX / 2,
                        getPixelBox().y + getPixelBox().height / 2 - headX / 2,
                        getPixelBox().x + getPixelBox().width / 2, offset_center);
                break;
            case NORTH:  // reverse strand
                this.stem.x = getPixelBox().x + (getPixelBox().width - stemWidth) / 2;
                this.stem.y = getPixelBox().y + headX;
                this.stem.width = stemWidth;
                this.stem.height = getPixelBox().height - headX;
                if(zoomed)
                drawArrowHead(g, getPixelBox().y + getPixelBox().height / 2 - headX / 2,
                        getPixelBox().y + getPixelBox().height / 2 + headX / 2,
                        getPixelBox().x + getPixelBox().width / 2, offset_center);
                break;
            default:
        }
        g.fillRect(this.stem.x, this.stem.y,this.stem.width, this.stem.height);
            super.draw(view);
    }
    private void drawArrowHead(Graphics g, int tip_x, int flat_x, int tip_center, int x_center) {

        switch (this.getOrientation()) {
            case NeoConstants.HORIZONTAL:
                x[0] = flat_x;
                y[0] = tip_center - headY / 2;
                x[1] = tip_x;
                y[1] = tip_center;
                x[2] = flat_x;
                y[2] = tip_center + headY / 2;
                x[3] = flat_x;
                y[3] = tip_center + headY / 4;
                x[4] = x_center;
                y[4] = tip_center;
                x[5] = flat_x;
                y[5] = tip_center - headY / 4;
                break;
            case NeoConstants.VERTICAL:
                y[0] = flat_x;
                x[0] = tip_center - headY / 2;
                y[1] = tip_x;
                x[1] = tip_center;
                y[2] = flat_x;
                x[2] = tip_center + headY / 2;
                y[3] = flat_x;
                x[3] = tip_center + headY / 4;
                y[4] = x_center;
                x[4] = tip_center;
                y[5] = flat_x;
                x[5] = tip_center - headY / 4;
                break;
        }
        if (fillArrowHead) {
            g.fillPolygon(x, y, 6);
        } else {
            g.drawPolygon(x, y, 6);
        }
    }

}
