package com.affymetrix.genoviz.glyph;
import com.affymetrix.genoviz.bioviews.ViewI;
import com.affymetrix.genoviz.util.NeoConstants;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

/**
 * An arrow glyph.
 */
public class ArrowHeadGlyph extends DirectedGlyph {
    private static final int buffer_pixel = 10;
    private int x[];
    private int y[];
    private int headX, headY;
    protected boolean fillArrowHead = true;
    public ArrowHeadGlyph() {
        x = new int[6];
        y = new int[6];
    }

    private void calHead() {
        switch (this.getOrientation()) {
            case NeoConstants.HORIZONTAL:
                this.headY = getPixelBox().height;
                break;
            case NeoConstants.VERTICAL:
                this.headY = getPixelBox().width;
                break;
        }
        this.headX = this.headY / 2;
    }

    @Override
    public void draw(ViewI view) {
        double hold_y = getCoordBox().y;

        getCoordBox().y += (getCoordBox().height / 2);
        view.transformToPixels(getCoordBox(), getPixelBox());
        int offset_center = getPixelBox().y;
        getCoordBox().y = hold_y;
        view.transformToPixels(getCoordBox(), getPixelBox());
        calHead();
        if (headY < 8 || getPixelBox().x + getPixelBox().width / 2 + headX / 2 + buffer_pixel > getPixelBox().x + getPixelBox().width
                || getPixelBox().x + getPixelBox().width / 2 - headX / 2 - buffer_pixel < getPixelBox().x) {
            return;
        }

        Graphics g = view.getGraphics();
        g.setColor(getBackgroundColor());

        switch (this.getDirection()) {
            case EAST:  // forward strand

                //bounds.setBounds(getPixelBox().x + getPixelBox().width/2 - headX/2, offset_center, headX, headY);
                drawArrowHead(g, getPixelBox().x + getPixelBox().width / 2 + headX / 2,
                        getPixelBox().x + getPixelBox().width / 2 - headX / 2,
                        offset_center, getPixelBox().x + getPixelBox().width / 2);
                break;
            case WEST:
                //bounds.setBounds(getPixelBox().x + getPixelBox().width/2 - headX/2, offset_center, headX, headY);
                drawArrowHead(g, getPixelBox().x + getPixelBox().width / 2 - headX / 2,
                        getPixelBox().x + getPixelBox().width / 2 + headX / 2,
                        offset_center, getPixelBox().x + getPixelBox().width / 2);
                break;
            case SOUTH:  // forward strand
                //bounds.setBounds(getPixelBox().x + getPixelBox().width/2 - headY/2, getPixelBox().x + getPixelBox().width/2, headY, headX);
                drawArrowHead(g, getPixelBox().y + getPixelBox().height / 2 + headX / 2,
                        getPixelBox().y + getPixelBox().height / 2 - headX / 2,
                        getPixelBox().x + getPixelBox().width / 2, offset_center);
                break;
            case NORTH:  // reverse strand
                //bounds.setBounds(getPixelBox().x + getPixelBox().width/2 - headY/2, getPixelBox().x + getPixelBox().width/2, headY, headX);
                drawArrowHead(g, getPixelBox().y + getPixelBox().height / 2 - headX / 2,
                        getPixelBox().y + getPixelBox().height / 2 + headX / 2,
                        getPixelBox().x + getPixelBox().width / 2, offset_center);
                break;
            default:
        }
        super.draw(view);
    }

    private void drawArrowHead(Graphics g, int tip_x, int flat_x, int tip_center, int x_center) {
        switch (this.getOrientation()) {
            case NeoConstants.HORIZONTAL:
                x[0] = flat_x;
                y[0] = tip_center - headY / 2;
                x[1] = tip_x;
                y[1] = tip_center;
                x[2] = flat_x;
                y[2] = tip_center + headY / 2;
                x[3] = flat_x;
                y[3] = tip_center + headY / 4;
                x[4] = x_center;
                y[4] = tip_center;
                x[5] = flat_x;
                y[5] = tip_center - headY / 4;
                break;
            case NeoConstants.VERTICAL:
                y[0] = flat_x;
                x[0] = tip_center - headY / 2;
                y[1] = tip_x;
                x[1] = tip_center;
                y[2] = flat_x;
                x[2] = tip_center + headY / 2;
                y[3] = flat_x;
                x[3] = tip_center + headY / 4;
                y[4] = x_center;
                x[4] = tip_center;
                y[5] = flat_x;
                x[5] = tip_center - headY / 4;
                break;
        }
        if (fillArrowHead) {
            g.fillPolygon(x, y, 6);
        } else {
            g.drawPolygon(x, y, 6);
        }
    }
}
