/*  Copyright (c) 2012 Genentech, Inc.
 *
 *  Licensed under the Common Public License, Version 1.0 (the "License").
 *  A copy of the license must be included
 *  with any distribution of this source code.
 *  Distributions from Genentech, Inc. place this in the IGB_LICENSE.html file.
 * 
 *  The license is also available at
 *  http://www.opensource.org/licenses/CPL
 */
/**
 * Provides classes pertaining to colors and color schemes in IGB. These can be
 * used to sensibly color related and contrasting data.
 */
package com.affymetrix.genoviz.color;
