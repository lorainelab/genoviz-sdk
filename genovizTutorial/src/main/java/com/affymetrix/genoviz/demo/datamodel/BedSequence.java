package com.affymetrix.genoviz.demo.datamodel;

import java.util.HashMap;
import java.util.Map;

public class BedSequence {
    int start = 0,end = 0;
    String chr,name,strand,description;
    Map seqMap;
    String[] seqG,laG,pG;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getChr() {
        return chr;
    }

    public void setChr(String chr) {
        this.chr = chr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map getSeqMap() {
        return seqMap;
    }

    public void setSeqMap(Map seqMap) {
        this.seqMap = seqMap;
    }

    public String[] getSeqG() {
        return seqG;
    }

    public void setSeqG(String[] seqG) {
        this.seqG = seqG;
    }

    public String[] getLaG() {
        return laG;
    }

    public void setLaG(String[] laG) {
        this.laG = laG;
    }

    public String[] getpG() {
        return pG;
    }

    public void setpG(String[] pG) {
        this.pG = pG;
    }

    public String getStrand() {
        return strand;
    }

    public void setStrand(String strand) {
        this.strand = strand;
    }
    public void createSeqMap(){
        seqMap = new HashMap<String,int[]>();
        int strt =start;
        int end = strt + Integer.parseInt(seqG[0]) + Integer.parseInt(laG[0]);
        seqMap.put("SequenceGlyph-0",new int[]{strt,end});
//        System.out.println("SG0,"+strt+","+end);
        boolean key = false;
//          @kraveend - In the bed file, the exon length and start points are provided.
//          Using these values, intron start should be +1 the prev exon's end point
//          and the end point should be -1 the next exon's start point

        for(int i=1; i<seqG.length;i++){
            strt = end;
            end = start+Integer.parseInt(laG[i]);
            seqMap.put("IntronGlyph-"+i, new int[]{strt+1, end-1});
//            System.out.println("IG"+"i,"+strt+","+end);
            strt = end;
            end =end + Integer.parseInt(seqG[i]);
            seqMap.put("SequenceGlyph-"+i, new int[]{strt,end});
//            System.out.println("SG"+strt+","+end);
        }

    }
}
