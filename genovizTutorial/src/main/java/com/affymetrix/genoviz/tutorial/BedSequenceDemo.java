package com.affymetrix.genoviz.tutorial;

import com.affymetrix.genoviz.awt.AdjustableJSlider;
import com.affymetrix.genoviz.bioviews.LinearTransform;
import com.affymetrix.genoviz.demo.datamodel.BedSequence;
import com.affymetrix.genoviz.demo.parser.BedFileParser;
import com.affymetrix.genoviz.event.NeoMouseEvent;
import com.affymetrix.genoviz.event.NeoRangeListener;
import com.affymetrix.genoviz.glyph.AxisGlyph;
import com.affymetrix.genoviz.util.NeoConstants;
import com.affymetrix.genoviz.widget.NeoMap;
import com.affymetrix.genoviz.widget.Shadow;
import com.affymetrix.genoviz.widget.VisibleRange;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class BedSequenceDemo extends JFrame {
    JPanel mapPanel;
    NeoMap map;
    HashMap<String,Vector<BedSequence>> bedVector;
    JProgressBar pb;
    JLabel progress;
    int map_length = 0;
    public BedSequenceDemo() throws HeadlessException {
        bedVector = createBedSequenceList();
        mapPanel = createMapPanel(NeoConstants.HORIZONTAL);
        pb = new JProgressBar();
        progress = new JLabel();
        JPanel control = new JPanel();
        Container cpane = this.getContentPane();
        cpane.setLayout(new BorderLayout());
        control.add(new JLabel("Render Progress: "));
        control.add(pb);
        control.add(progress);
        cpane.add(control,"South");
        cpane.add(mapPanel);
    }
    public HashMap<String,Vector<BedSequence>> createBedSequenceList(){
        String bedUrl = "./genovizTutorial/src/main/resources/data/AraportBedFile.bed";
        BedFileParser bedFileParser = new BedFileParser(bedUrl);
        bedVector =bedFileParser.getResults();
        map_length = BedFileParser.getMapLength();
        return bedVector;
    }
    public Vector getChromosome(String chr){
        return bedVector.get(chr);
    }
    public void renderGeneModels(String chr,String strand){
        Vector chrSequence = getChromosome(chr+strand);
        int total = chrSequence.size();
        final int[] c = {0};
        long starttime = System.nanoTime();
        chrSequence.forEach(obj -> {
            BedSequence bs = (BedSequence)obj;
            updateProgress(starttime,++c[0],total);
            if(bs.getStrand().equals(strand)) {
                for (Map.Entry<String, int[]> entry : (Iterable<Map.Entry<String, int[]>>) bs.getSeqMap().entrySet()) {
                    String glyphtype = "-glyphtype " + entry.getKey().split("-")[0] + " -color blue -offset 40 -width 10";
                    int[] startEnd = entry.getValue();
                    map.configure(glyphtype);
                    map.addItem(startEnd[0], startEnd[1]);

                }
            }
            map.repaint();
        });
        updateProgress(starttime,c[0],total);
    }
    public void updateProgress(long starttime,int c,int total){
        pb.setValue(c*100/total);
        progress.setText(c *100/total+"%  "+ ((System.nanoTime()-starttime)/1000000000)+"s");
    }
    public JPanel createMapPanel(int orient) {
        map = new NeoMap(true, true, orient, new LinearTransform());
        final VisibleRange selectedRange = new VisibleRange();
        map.setBackground(new Color(255,255,255));
        map.setSelectionEvent(NeoMap.ON_MOUSE_DOWN);
        map.setMapRange(0, map_length);
        map.setMapOffset(0, 250);
        AxisGlyph ax = map.addAxis(50);
        ax.setForegroundColor(new Color(71,68,68));
        AdjustableJSlider xzoomer = new AdjustableJSlider(Adjustable.HORIZONTAL);
        map.setZoomer(NeoMap.X, xzoomer);
        AdjustableJSlider yzoomer = new AdjustableJSlider(Adjustable.VERTICAL);
        map.setZoomer(NeoMap.Y, yzoomer);
        Shadow hairline = new Shadow(map, orient);
        selectedRange.addListener(hairline);
        hairline.label.setFont(new Font("Courier", Font.PLAIN, 20));

        if (orient == NeoConstants.HORIZONTAL) {
            map.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    selectedRange.setSpot(((NeoMouseEvent) e).getCoordX());
                }
            });
        } else {
            map.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    selectedRange.setSpot(((NeoMouseEvent) e).getCoordY());
                }
            });
        }
        final int axisID = (orient == NeoConstants.HORIZONTAL) ? NeoMap.X : NeoMap.Y;
        NeoRangeListener zoomMidPointSetter = e -> {
            double midPoint = (e.getVisibleEnd() + e.getVisibleStart()) / 2.0;
            map.setZoomBehavior(axisID, NeoMap.CONSTRAIN_COORD, midPoint);
            map.updateWidget();
        };
        selectedRange.addListener(zoomMidPointSetter);

        JPanel map_pan = new JPanel();
        //NeoPanel map_pan = new NeoPanel();
        map_pan.setLayout(new BorderLayout());
        map_pan.add("Center", map);
        map_pan.add("West", yzoomer);
        map_pan.add("North", xzoomer);

        return map_pan;
    }

    static public void main(String[] args) {
        BedSequenceDemo demo = new BedSequenceDemo();
        demo.setTitle("BedSequenceDemo");
        demo.setMinimumSize(new Dimension(1000,500));
        demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        demo.pack();
        demo.setVisible(true);
        demo.renderGeneModels("Chr5","-");
    }
}
